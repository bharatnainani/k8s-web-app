
# IAM role with no permission attached
resource "aws_iam_role" "energi_role" {
  name = var.role_name
  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = "sts:AssumeRole"
        Effect = "Allow"
        Sid    = ""
        Principal = {
          Service = "ec2.amazonaws.com"
        }
      },
    ]
  })
}

# IAM policy that is assuming the role

resource "aws_iam_policy" "energi_policy" {
  name        = var.policy_name
  description = "Iam policy for users"
  policy = jsonencode({
    Version = "2012-10-17",
    Statement = [
      {
        Effect   = "Allow",
        Action   = "sts:AssumeRole",
        Resource = aws_iam_role.energi_role.arn
    }]
  })
}

# Create IAM Group

resource "aws_iam_group" "energi_iam_group" {
  name = var.group_name
}

# Attach policy to Group

resource "aws_iam_group_policy_attachment" "attach_policy" {
  group      = aws_iam_group.energi_iam_group.name
  policy_arn = aws_iam_policy.energi_policy.arn
}

# Create IAM user 

resource "aws_iam_user" "energi_user" {
  name = var.user_name
}

# Add user to IAM Group

resource "aws_iam_user_group_membership" "adding_user" {
  user = aws_iam_user.energi_user.name

  groups = [
    aws_iam_group.energi_iam_group.name,
  ]
}
