terraform {
  required_version = "~> 1.0"
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }
}

provider "aws" {
  region = var.region
  default_tags {
    tags = {
      Environment = var.environment
      Project     = var.project
    }
  }
}

data "aws_caller_identity" "current" {}
# we can also add terraform backend for S3 to save the file

# terraform {
#   backend "s3" {
#     bucket         = "terraform-state-bucket-915599166308"
#     key            = "ecs-infra/terraform.tfstate"
#     region         =  "ap-south-1"
#     encrypt        =  true
#   }
# }
