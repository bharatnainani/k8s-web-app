# Energi3-app

## Overview
Question 1, Question 2, Question 3
This project will deploy  appliction on AWS EKS using gitlab CI/CD. 

## Prerequisites
You should have a basic understanding of [Kubernetes](https://kubernetes.io/) like, Deployments , Services, Secret, ServiceAccount etc.

## Deployment

First we need to deploy  the EKS cluster on AWS. we are using [eksctl](https://eksctl.io/) utility which makes it really easy to set up an AWS [EKSCluster](https://aws.amazon.com/eks/) .

### Install Eksctl
```
curl --silent --location "https://github.com/weaveworks/eksctl/releases/latest/download/eksctl_$(uname -s)_amd64.tar.gz" | tar xz -C /tmp
sudo mv /tmp/eksctl /usr/local/bin
```

### Deploy EKS Cluster
```
eksctl create cluster --name=energi3 --nodes=1 --node-type t3.small
```
This will take around 10 to 15 minutes. Once the cluster is created, you can set up your kubeconfig file using the AWS CLI

```
aws eks update-kubeconfig --name energi3 --region <region>
```

### Deploy Service Account

It is use to deploy to Kubernetes from GitLab
```
kubectl apply -f gitlab-service-account.yaml
```

### ADD ENViRONMENT VARIABLES in GitLab
**DOCKER_USER**. This is the Docker user you use to login to the Docker Hub.

**DOCKER_PASSWORD**. This is the Docker passwrod you use to login to the Docker Hub.

**CERTIFICATE_AUTHORITY_DATA**. This is the CA configuration for the Kubernetes cluster. For EKS, login to the AWS EKS console and open up your cluster configuration. You can find the Certificate Authority on the right.

**SERVER**. This is the endpoint to the Kubernetes API for our cluster. You can find this on the page where you already are.

**USER_TOKEN**. This is the token for the user that we'll use to connect to the Kubernetes cluster. We need to find the token for the user that we created earlier. First, list all secrets with kubectl get secrets. There will be a secret starting with **gitlab-service-account-token**, which is the token for the GitLab user we created earlier. Copy the NAME for this secret, and run the following command to see the token: ```kubectl describe secret [NAME]```. Copy the token that is part of the output, and enter it in GitLab.

### Run CI/CD pipeline

You can manually run it or you can  push code to reposistory it will be automatically triggered
This pipeline will build and deploy the code

